/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * testapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Qt.labs.platform 1.0
import QtSystemInfo 5.5

import Example 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'testapp.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Test App')
        }

        DeviceInfo {
            id: deviceInfos
        }

        NetworkInfo {
            id: networkInfo
        }

        Flickable {
            contentHeight: units.gu(105)

            anchors {
                topMargin: header.height
                fill: parent
            }

            ColumnLayout {
                id: mainColumn
                spacing: units.gu(2)

                anchors {
                    fill: parent
                    margins: units.gu(2)
                }

                Label {
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignHCenter

                    text: i18n.tr('Version ') + Qt.application.version
                     + i18n.tr('\n i18n tests ')
                     + i18n.dctr("testapp.cibersheep","context",'\n Text - test dctr')
                     + "\n"
                     + i18n.dtr("testapp.cibersheep","\nONE",'\n TWO - test dtr', 2)
                     + "\n"
                     + i18n.tag("CONTEXT",'This is a tag')
                     + i18n.tr('This is a tag')
                     + "\n"
                     + i18n.tr('\n Name ') + Qt.application.name
                     + i18n.tr('\n platform ') + Qt.platform.os
                     + i18n.tr('\n pluginName ') + Qt.platform.pluginName
                     + i18n.tr('\n uiLanguage ') + Qt.uiLanguage
                     + i18n.tr('\n Qt.Locale().name ') + Qt.locale().name

                     + i18n.tr('\n Device info ') + deviceInfos.version(DeviceInfo.Os) + " " + deviceInfos.hasFeature(DeviceInfo.FmRadioFeature)
                     + i18n.tr('\n Network info ') + networkInfo.monitorNetworkName

                     + i18n.tr('\n\n w CacheLocation ') + StandardPaths.writableLocation(StandardPaths.CacheLocation)
                     + i18n.tr('\n s CacheLocation ') + StandardPaths.standardLocations(StandardPaths.CacheLocation)
                     + i18n.tr('\n w ApplicationsLocation ') + StandardPaths.writableLocation(StandardPaths.ApplicationsLocation)
                     + i18n.tr('\n s ApplicationsLocation ') + StandardPaths.standardLocations(StandardPaths.ApplicationsLocation)
                     + i18n.tr('\n w TempLocation ') + StandardPaths.writableLocation(StandardPaths.TempLocation)
                     + i18n.tr('\n s TempLocation ') + StandardPaths.standardLocations(StandardPaths.TempLocation)
                     + i18n.tr('\n w RuntimeLocation ') + StandardPaths.writableLocation(StandardPaths.RuntimeLocation)
                     + i18n.tr('\n s RuntimeLocation ') + StandardPaths.standardLocations(StandardPaths.RuntimeLocation)
                     + i18n.tr('\n w HomeLocation ') + StandardPaths.writableLocation(StandardPaths.HomeLocation)
                     + i18n.tr('\n s HomeLocation ') + StandardPaths.standardLocations(StandardPaths.HomeLocation)
                     + i18n.tr('\n w GenericDataLocation ') + StandardPaths.writableLocation(StandardPaths.GenericDataLocation)
                     + i18n.tr('\n s GenericDataLocation ') + StandardPaths.standardLocations(StandardPaths.GenericDataLocation)

                    wrapMode: Text.Wrap
                }

                Label {
                    id: label
                    Layout.alignment: Qt.AlignHCenter
                    text: i18n.tr('Press the button below and check the logs!')
                }

                Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: i18n.tr('Press here!')
                    onClicked: Example.speak()
                }
            }
        }
    }

    //Handle incoming urldispatcher test:// url
    Connections {
        target: UriHandler

        onOpened: {
            console.log("UriHandler signal triggered-------------------")

            if (uris.length > 0) {
                console.log("uri received:", uris[0])
            }
        }
    }

    Component.onCompleted: {
        console.log("onCompleted-----------------------")
        console.log(" Check cpp Stardard Paths        -")
        Example.systemInfo();

        console.log("\n"," Check urldispatcher             -")
        if (Qt.application.arguments) {
            console.log("Qt.application.arguments exists")
            if (Qt.application.arguments.length > 0) {
                console.log("Qt.application.arguments is > 0")
                for (var i = 0; i < Qt.application.arguments.length; i++) {
                    console.log("Qt.application.arguments",i,"is",Qt.application.arguments[i])
                }
            }
        }
    }
}
