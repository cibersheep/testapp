/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * testapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QString>
#include <QStandardPaths>

#include "example.h"

Example::Example() {

}

void Example::speak() {
    qDebug() << "hello world!";
}

void Example::systemInfo() {
    qDebug() << "CacheLocation (w):        " << QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    qDebug() << "AppConfigLocation (w):    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "ConfigLocation (w):       " << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    qDebug() << "AppConfigLocation (w):    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "TempLocation (w):         " << QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    qDebug() << "AppLocalDataLocation (w): " << QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
}
